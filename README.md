# **Bitstring Suite by Redhart** #

This is an utility software that targets ease in base conversions and encoding/decoding as well as a special crypt I wrote.

While working with databasea, you may want to encode/decode values in any of the standard encoding formats. Sometimes you just want to send a text-based package on lightweight encryption over a server to a friend or teammate.

Bitstring Suite covers these and more.

The UI speaks for itself.


## **Screenshots** ##

![14-30-36.png](https://bitbucket.org/repo/bzjkxn/images/2980403865-14-30-36.png)

![14-32-14.png](https://bitbucket.org/repo/bzjkxn/images/3193642233-14-32-14.png)

![14-33-16.png](https://bitbucket.org/repo/bzjkxn/images/768011559-14-33-16.png)


That's about it.
TIP: Fire it up faster by sticking it to your taskbar.